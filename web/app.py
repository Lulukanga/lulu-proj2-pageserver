from flask import Flask, abort, render_template, request
import os

app = Flask(__name__)


def find_evil(name: str) -> bool:
    """Catch malicious routes that have //, ~ or .. in them.
    Args:
        route: A string
    Returns:
        False: if malicious URL was intercepted
        True: if we're all good in the hood
        None: if no route was received (Not implemented error 401)
    """
    result = True
    # print("looking for evil in:", name)
    dub_slashes = name.split("//")
    if len(dub_slashes) > 1:
        # end1 = dub_slashes[-1]
        result = False
    else:
        sing_slashes = name.split("/")
        # print("sing_slashes is >>>:", sing_slashes)
        if sing_slashes[-1] == "":
            result = None
        else:
            end = sing_slashes[-1]
            if (end[0] == "~") or (end[0:2] == ".."):
                result = False
            elif (len(sing_slashes) >= 3) and (sing_slashes[-2] == "") and (sing_slashes[-3] == ""):
                result = False
    return result


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


# @app.before_request
# def before_request_func():
#     search_term = str(request.path)
#     print('Search term:', search_term)


@app.errorhandler(403)
def forbidden(error):
    return render_template('403.html'), 403


@app.route('/<name>')  # TODO: @app.route('pages/<name>')?
def details(name):
    print(">>>details", name)
    return handle_request(name)


@app.route('/<root>/<name>')  # TODO: @app.route('root/<name>')?
def details2(root, name):
    return handle_request(name)


@app.route('/<root>/<name>/<name2>')  # TODO: @app.route('root//<name>')
def details3(root, name):
    return handle_request(name)


# Index page, no args
# This decorator ties the root URL to the pages() function. So when a user goes to the root URL http://localhost:5000/,
# the pages() function is automatically invoked.
@app.route('/')
def index():
    # Retrieves the name parameter from URL, e.g., if you specify a URL like http://localhost:5000/?nameRequest=Bla,
    # the name variable (in python) will contain the value "Bla"
    name = request.args.get("nameRequest")
    raw_uri = request.environ['RAW_URI']
    # print("We're in index() now, name is ", name, "and raw uri is", raw_uri)
    if name is None and raw_uri == "/":
        name = "Landing"
        return render_template("landing.html", nameHtml=name, content=name)  # sends your data to a template and returns the rendered HTML to the browser.
    else:
        return handle_request(name)


@app.before_request
def before_request_func():
    # print("before_request is running!")
    raw_uri = request.environ['RAW_URI']  #TODO: documentation/ref!
    # print("raw uri is >>>", raw_uri)
    ok = find_evil(raw_uri)
    # print("find evil returned ", ok)
    if ok is None:
        name = "Landing"
        return render_template("landing.html", nameHtml=name, content=name)
        # index()
    if not ok:
        abort(403)


def handle_request(name):
    raw_uri = request.environ['RAW_URI']
    exists = os.path.exists(f"./templates/{name}")
    if not exists:
        abort(404)
    elif exists:
        return render_template(f"{name}", content=name) # MUST be returned!


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)  # TODO: The example given uses these args, but should I have port=5000?

