# README: proj2-pageserver

**Author**: Lindsey Uribe

**Contact**: luribe@uoregon.edu

## A "trivial server" for trivial things.

### What is this repository for?

This repo contains a simple server that utilizes Flask to serve up very simple 
HTML and CSS pages from a single directory. Appropriate responses are served up 
for malicious URL attempts as well as non-existent pages. 

In addition to page serving, this repo contains instructions for building 
a Docker image with all of the essential components needed to run the simple server. 